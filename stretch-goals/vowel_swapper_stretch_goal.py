def vowel_swapper(string):
    # ==============
    # Your code here

	equivalence= {"a":4,"A":4, "e": 3, "E": 3, "I":"!", "i":"!", "o": "ooo", "O": "000", "U":"|_|", "u":"|_|"}


	list_equiv = []
	list_string= list (string)
	count= 0 


	for cht in list_string:
		count += 1  
		if cht in equivalence:   
			list_equiv.append (equivalence[cht])
			if cht.upper() != "O": 
				if list_equiv.count(equivalence[cht])==2:
					list_string[count-1] = str(equivalence[cht])
			else: 
				if list_equiv.count("ooo") + list_equiv.count ("000")==2:
					list_string[count-1] = str(equivalence[cht])
					

	string = "".join(list_string)
	return string 	
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
